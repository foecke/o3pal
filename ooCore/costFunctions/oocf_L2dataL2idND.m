
classdef oocf_L2dataL2idND < oooRoot
    
    methods (Access = public)
        
        
        function out = costFunction(obj, x)
            out = (0.5 .* norm(x(:)-obj.i.b).^2 + 0.5 .* obj.p.alpha .* norm(x(:)).^2) ./ numel(x(:));
        end
        
        
    end
    
end


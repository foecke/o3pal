
classdef oocf_L2odataL2idND < oooRoot
    
    methods (Access = public)
        
        
        function out = costFunction(obj, x)
            out = (0.5 .* norm(obj.i.A*x(:)-obj.i.b).^2 + 0.5 .* obj.p.alpha .* norm(x(:)).^2) ./ numel(x(:));
        end
        
        
    end
    
end

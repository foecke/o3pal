
classdef oocf_L2dataL1idND < oooRoot
    
    methods (Access = public)
        
        
        function out = costFunction(obj, x)
            out = ( 0.5 .* norm(x(:)-obj.i.b).^2 + obj.p.alpha .* norm(x(:), 1) ) ./ numel(x(:));
        end
        
        
    end
    
end

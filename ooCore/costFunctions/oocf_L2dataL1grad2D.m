
classdef oocf_L2dataL1grad2D < oooRoot
    
    methods (Access = public)
        
        
        function out = costFunction(obj, x)
            out = ( 0.5 .* norm(x(:)-obj.i.b).^2 + obj.p.alpha .* norm(obj.i.gradX*x(:)+obj.i.gradY*x(:), 1) ) ./ numel(x(:));
        end
        
        
    end
    
end

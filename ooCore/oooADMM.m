classdef oooADMM < handle & oooIterative
       
    methods (Access = public)
        function obj = oooADMM()
            obj@oooIterative();
        end
    end
        
    methods (Access = protected)
        
        function obj = initializeAlg(obj)  
            obj.s.tolPrimalDual         = 1e-5;
            obj.i.stopMessages(6)       = 'Hit primal-dual tolerance';
            
            obj.s.adaptiveSteps = true;
            obj.s.adaptiveWeight = 0.1;
            
            obj.o.primalErr     = Inf;
            obj.o.dualErr       = Inf;
            obj.o.nnegErr       = Inf;
            
            obj.s.verbose.pattern= '<name>: <iter> - <adaptive> - <cost> - <primal> - <dual> - <printtime> - <time>';
        end
                
        function obj = routineCheckStopCrit(obj)
            obj.routineCheckStopCrit@oooIterative();
            if obj.o.primalErr < obj.s.tolPrimalDual && obj.o.dualErr < obj.s.tolPrimalDual
                obj.i.stop = 6;
            end
        end
        
        
        function obj = recordCustomFunctions(obj)
            obj.o.record(obj.i.record.iter).nnegErr     = obj.o.nnegErr;
            obj.o.record(obj.i.record.iter).primalErr	= obj.o.primalErr;
            obj.o.record(obj.i.record.iter).dualErr     = obj.o.dualErr;
        end
        
        
        function obj = parseVerbosePattern(obj)            
            parseVerbosePattern@oooIterative(obj);
            
            obj.i.verbose.string = strrep(obj.i.verbose.string, '<primal>', sprintf('P: %06i', obj.o.primalErr));
            obj.i.verbose.string = strrep(obj.i.verbose.string, '<dual>'  , sprintf('D: %06i', obj.o.dualErr));
        end
        
        
        function obj = adaptiveStep(obj)
            obj.i.rhoOld = obj.p.rho;
            if obj.o.primalErr>5*obj.o.dualErr
                obj.p.rho = obj.p.rho*(1+obj.s.adaptiveWeight);
                obj.i.adaptive.string = [obj.i.adaptive.string,'^'];
            elseif obj.o.dualErr>5*obj.o.primalErr
                obj.p.rho = obj.p.rho*(1-obj.s.adaptiveWeight);
                obj.i.adaptive.string = [obj.i.adaptive.string,'v'];
            else
                obj.i.adaptive.string = [obj.i.adaptive.string,' '];
            end
        end
        
    end
    
    methods (Access = protected, Abstract)
        
        setEffectiveA(obj);
        setRightHandSide(obj);        
        
    end
end

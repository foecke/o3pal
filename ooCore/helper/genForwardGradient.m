function gradientComplete = genForwardGradient(res,stepsize)
    gradientComplete = {};
    
    for i=1:numel(stepsize)
        
        gradMat = 1/stepsize(i) .* spdiags([-ones(res(i), 1) ones(res(i), 1)], 0:1, res(i), res(i));
        gradMat(end,:) = 0;
        
        if (i==1)
            grad = gradMat;
        else
            grad = speye(res(1));
        end
        
        for j=2:numel(stepsize)
            if (j==i)
                grad = kron(gradMat,grad);
            else
                grad = kron(speye(res(j)),grad);
            end
        end
        gradientComplete{i} = grad;
    end
end

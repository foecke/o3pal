
%   Author:     Lea Foecke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen
%   Date:       13-Oct-2017

function vol = vec2vol(vec, dim)
	vol = reshape(vec, dim);
end
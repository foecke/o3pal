classdef oooIterative < handle & oooRoot
    
    methods (Access = public)
        function obj = oooIterative()
            obj@oooRoot();
        end
        
        
        function obj = run(obj, doIterations)
            obj.setParameterString();
            obj.printOpening();
            obj.openLogFile();
            
            obj.i.stop = 0;
            
            if nargin < 2
                obj.i.doIterations = obj.s.maxIter-obj.o.iteration;
            else
                obj.i.doIterations = doIterations;
            end
            
            obj.routineCheckStopCrit();
            if ~obj.i.stop
                obj.routineStartup();
                obj.i.overallTime   = tic;
                obj.i.iterTime      = tic;
                obj.evaluateCostFunction();
                while obj.i.stop == 0
                    obj.routineUpdateIteration();
                    obj.routineUpdateStep();               
                    obj.evaluateCostFunction();
                    obj.routineAdaptiveStep();
                    obj.routineCheckStopCrit();
                    obj.routinePrint();
                    obj.routineVisualization();     
                    obj.routineRecord();
                end
                obj.routineFinish();
            end
            obj.routineVisualization();
            obj.routineRecord();
            obj.closeLogFile();
            obj.printClosing();
        end
        
        function obj = setStartValue(obj, x0)
            obj.o.result    = x0(:);
            obj.o.currCost  = obj.costFunction(obj.o.result);
        end
        
        function status = getStatus(obj)
            status = obj.i.stop;
        end
        
        function obj = getStatusText(obj)
            printOutput(obj, 0, '');
            printOutput(obj, 0, '%s DONE (after %i iterations): %s!', obj.getAlgoName(), obj.o.iteration, obj.i.stopMessages(obj.i.stop));
        end
        
        function obj = reset(obj)
            obj.o.iteration             = 0;
            obj.o.currCost              = 1;
            obj.i.costOld               = Inf;
            obj.i.verbose.iter           = 0;
            obj.i.adaptive.string        = '';
            obj.o.record                 = [];     
        end
        
        
    end
    
    methods (Access = public,Abstract)
        
        out = costFunction(obj, x);
        
    end
    
    methods (Access = protected)
        
        function obj = initializeObj(obj)
            
            obj.initializeObj@oooRoot();
            obj.reset();
            
            obj.i.stop                  = 0;
            
            obj.i.iterTime              = 0;
            obj.s.maxIter               = 100000;
            
            obj.s.tolAbs                = 1e-6;  %tolerance of cost
            obj.s.tolRel                = 1e-10; %tolerance of (cost - costOld)
            
            obj.s.verbose.interval       = 1;
            obj.i.verbose.defaultPattern = '<name>: <iter> - <adaptive> - <cost> - <printtime> - <time>';
            obj.s.verbose.pattern        = obj.i.verbose.defaultPattern;
            obj.s.verbose.adaptive       = true;
            obj.s.verbose.adaptiveInterval = [0.8, 1.2];
                        
            obj.i.record.iter           = 1;
            obj.s.record.interval       = 10;
            obj.s.record.result         = false;
            obj.s.record.cost           = true;
            obj.s.record.ssim           = false;
            obj.s.record.psnr           = false;
            obj.s.record.relErr         = false;
            obj.s.record.approxErr      = false;   
            
            obj.s.adaptive.interval     = obj.s.record.interval;
            
            obj.s.vis.interval          = 0;    
            obj.s.vis.export.interval   = 0;
            
            obj.i.stopMessages          = containers.Map(0,'undefined reason');
            obj.i.stopMessages(1)       = 'Not Finished';
            obj.i.stopMessages(2)       = 'Hit max iteration';
            obj.i.stopMessages(3)       = 'Hit absolute tolerance';
            obj.i.stopMessages(4)       = 'Hit relative tolerance';
            obj.i.stopMessages(5)       = 'Algorithm did not converge';
            
            
        end
        
        function obj = routineUpdateIteration(obj)
            obj.i.doIterations  = obj.i.doIterations-1;
            obj.o.iteration     = obj.o.iteration+1;
            obj.i.verbose.iter  = obj.i.verbose.iter+1;
        end
        
        function obj = routineFinish(obj)
            if obj.i.stopMessages.isKey(obj.i.stop)
                obj.printOutput(2, '');
                obj.printOutput(2, '%s DONE (after %i iterations): %s!', obj.getAlgoName(), obj.o.iteration, obj.i.stopMessages(obj.i.stop));
            else
                obj.printOutput(2, '');
                obj.printOutput(2, '%s DONE (after %i iterations): %s!', obj.getAlgoName(), obj.o.iteration, obj.i.stopMessages(0));
            end
        end
        
        
        function obj = routinePrint(obj)
            
            if mod(obj.i.verbose.iter,obj.s.verbose.interval) == 0 || obj.i.stop > 0
                obj.i.verbose.iter = 0;
                %%% use output pattern
                obj.i.verbose.string = [obj.i.verbose.string,obj.s.verbose.pattern];
                
                parseVerbosePattern(obj);
                
                %%% print and reset verboseStrings
                printOutput(obj, 1, obj.i.verbose.string);
                obj.i.adaptive.string    = '';
                obj.i.verbose.string     = '';
                
                %%% check adaptive verbose
                if obj.s.verbose.adaptive
                    if obj.i.curIterTime < obj.s.verbose.adaptiveInterval(1)
                        obj.s.verbose.interval = ceil(1.1.*obj.s.verbose.interval);
                    elseif obj.i.curIterTime > obj.s.verbose.adaptiveInterval(2)
                        obj.s.verbose.interval = max(1, ceil(0.9.*obj.s.verbose.interval));
                    end
                end
            end
        end
        
        function obj = routineCheckStopCrit(obj)
            if obj.i.doIterations == 0
                obj.i.stop = 1;
            end
            if obj.o.iteration >= obj.s.maxIter
                obj.i.stop = 2;
            end
            if obj.o.currCost < obj.s.tolAbs
                obj.i.stop = 3;
            end
            if obj.i.record.iter > 1 && abs(obj.o.currCost-obj.i.costOld) < obj.s.tolRel
                obj.i.stop = 4;
            end
            if obj.o.currCost > 1e100
                obj.i.stop = 5;
            end
        end
        
        
        function obj = routineAdaptiveStep(obj)
            if mod(obj.o.iteration,obj.s.adaptive.interval) == 0
                obj.adaptiveStep();
            end
        end
        
        function obj = adaptiveStep(obj)
        end
        
        function obj = routineVisualization(obj)
            
            if mod(obj.o.iteration, obj.s.vis.interval) == 0
                obj.doVisualization();
            end
            if obj.i.stop && obj.s.vis.interval > 0
                obj.doVisualization();
            end
            
            if mod(obj.o.iteration, obj.s.vis.export.interval) == 0
                obj.saveVisualization(sprintf(['_iter%0',num2str(log10(obj.s.maxIter)+1),'.0f'],obj.o.iteration));
            end
            if obj.i.stop && obj.s.vis.export.interval > 0
                obj.saveVisualization();
            end            
        end
        
        function obj = parseVerbosePattern(obj)
            
            parseVerbosePattern@oooRoot(obj);
            
            obj.i.curIterTime = toc(obj.i.iterTime);
            if obj.s.verbose.adaptive
                obj.i.verbose.string = strrep(obj.i.verbose.string, '<printtime>'    , sprintf('iter/print: %i', obj.s.verbose.interval));
            else
                obj.i.verbose.string = strrep(obj.i.verbose.string, '<printtime>'    , sprintf('time/print: %.5fs', obj.i.curIterTime));
            end
            obj.i.iterTime = tic;
            
            if obj.s.adaptive.interval > 0
                obj.i.verbose.string = strrep(obj.i.verbose.string, '<adaptive>', sprintf(['%',num2str(ceil(obj.s.verbose.interval/obj.s.adaptive.interval)),'s'], obj.i.adaptive.string));
            else
                obj.i.verbose.string = strrep(obj.i.verbose.string, '<adaptive>', '');
            end
            
            obj.i.verbose.string = strrep(obj.i.verbose.string, '<iter>'    , sprintf(['Iter: %0',num2str(floor(log10(obj.s.maxIter))+1),'i'], obj.o.iteration));
            obj.i.verbose.string = strrep(obj.i.verbose.string, '<cost>'    , sprintf('Cost: %06i', obj.o.currCost));
            if ~isempty(obj.o.record)
                if isfield(obj.o.record, 'relErr')
                    obj.i.verbose.string = strrep(obj.i.verbose.string, '<recRelErr>'    , sprintf('RelErr: %06i', obj.o.record(end).relErr));
                end
                if isfield(obj.o.record, 'approxErr')
                    obj.i.verbose.string = strrep(obj.i.verbose.string, '<recApproxErr>'    , sprintf('ApproxErr: %06i', obj.o.record(end).approxErr));
                end
            end
        end
                
        
        function obj = routineRecord(obj)
            if ~isempty(obj.s.record.interval)
                if obj.i.stop || mod(obj.o.iteration,obj.s.record.interval) == 0
                    if isempty(obj.o.record) || obj.o.record(end).iter ~= obj.o.iteration
                        obj.o.record(obj.i.record.iter).iter = obj.o.iteration;

                        obj.recordResult();
                        obj.recordCost();
                        obj.recordSSIM();
                        obj.recordPSNR();
                        obj.recordRelErr();
                        obj.recordApproxErr();
                        obj.recordCustomFunctions();

                        obj.i.record.iter = obj.i.record.iter + 1;
                    end
                end
            end
        end
        
        function obj = recordCost(obj)
            if obj.s.record.cost
                obj.o.record(obj.i.record.iter).cost        = obj.o.currCost;
            end
        end
        function obj = recordResult(obj)
            if obj.s.record.result
                obj.o.record(obj.i.record.iter).result    	= obj.getResult();
            end
        end
        function obj = recordSSIM(obj)
            if obj.s.record.ssim
                obj.o.record(obj.i.record.iter).ssim     	= ssim(obj.getResult(), obj.getReferenceResult());
            end
        end
        function obj = recordPSNR(obj)
            if obj.s.record.psnr
                obj.o.record(obj.i.record.iter).psnr     	= psnr(obj.getResult(), obj.getReferenceResult());
            end
        end
        function obj = recordRelErr(obj)
            if obj.s.record.relErr
                obj.o.record(obj.i.record.iter).relErr    	= (obj.o.currCost - obj.costFunction(obj.getReferenceResult())) / obj.costFunction(obj.getReferenceResult());
            end
        end
        function obj = recordApproxErr(obj)
            if obj.s.record.approxErr
                obj.o.record(obj.i.record.iter).approxErr  	= norm(obj.o.result(:) - obj.getReferenceResultVec()) / norm(obj.getReferenceResultVec());
            end
        end
        function obj = recordCustomFunctions(obj)
%             obj.o.record(obj.i.record.iter).custom   	= 0;
        end
                
    end
end

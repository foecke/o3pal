%   Author:     Lea Foecke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen
%   Date:       13-Oct-2017

classdef oooRoot < handle
    
    properties (Access = public)
        p; %parameter
        s; %settings
        o; %outputs
    end
    properties (Access = protected)
        i; %internals
    end
    
    methods (Access = public)
        
        function obj = oooRoot()
            obj.initializeObj();
        end
        
        
        function obj = run(obj)
            obj.setParameterString();
            obj.printOpening();
            obj.openLogFile();
            
            obj.i.overallTime = tic;
            obj.routineStartup();
            obj.routineUpdateStep();             
            obj.evaluateCostFunction();
            obj.routinePrint();
            obj.routineFinish();
            obj.routineVisualization();
            
            obj.closeLogFile();
            obj.printClosing();
        end
        
        
    end
    
    methods (Access = protected,Abstract)
        
        initializeAlg(obj);
        routineUpdateStep(obj);
        
    end
    
    
    methods (Access = protected)
        
        function obj = initialize(obj, varargin)
            
            obj.printInitOpening();
            obj.initializeAlg();
            obj.parseOptions(varargin{:});
            obj.printInitClosing();
        end
        
        function obj = parseOptions(obj, varargin)
            
            ip = inputParser;
            ip.addParameter('p', []);
            ip.addParameter('s', []);
            ip.parse(varargin{:});
            tempP = ip.Results.p;
            tempS = ip.Results.s;
             
            if ~isempty(tempP)
                obj.p = obj.joinStructs(obj.p, tempP);
            end
            if ~isempty(tempS)
                obj.s = obj.joinStructs(obj.s, tempS);
            end
            
        end
                
        
        function obj = initializeObj(obj)
            obj.i.algName               	= 'unnamed algorithm';
            
            obj.o.result                    = [];
            obj.s.resultScaling             = 1; % multiplicative factor of the result
            obj.s.refResult                 = [];
            obj.i.startTime                 = now;
            
            obj.s.log.filePath              = []; %no log if no filepath is given
            obj.i.log.fileID                = 0; %leerer file identifier
            
            obj.s.verbose.mode              = 2; % 0=none, 1=minimal, 2=normal, 3=chatty, 4=debug
            obj.s.verbose.prefix            = ''; %ein präfix?
            obj.i.verbose.defaultPattern    = '<overall>';
            obj.s.verbose.pattern           = obj.i.verbose.defaultPattern; %was so?
            obj.i.verbose.string            = ''; %verboseString sammelt alles ein während des durchlaufs...
            
            obj.s.eval                      = ''; %soll was ausgerechnet werden?    
            
            obj.s.vis.export.format         = 'png'; %FIG, PNG, PDF (or multiple comma separated)
            obj.s.vis.export.name           = ''; %defaults to ['recon_', obj.getUniqueName()]
            obj.s.vis.export.path           = ''; %defaults to current folder
            obj.s.vis.export.interval       = 0; 
            obj.i.vis.export.name           = ''; %internal temporary used filename
            obj.i.vis.export.fid            = [];
            obj.s.vis.fid                   = [];
            obj.s.vis.axes                  = [];
            obj.s.vis.showColorbar          = false;
            obj.s.vis.visible               = true;
            obj.s.vis.cmd                   = [];
            
            obj.s.useHelperFiles            = true;
            
            obj.o.currCost                  = 1;
            obj.i.costOld                   = Inf;
        end
        
        function obj = printInitOpening(obj)
            obj.printOutput(2, '');
            obj.printOutput(2, '%s', repmat('=', 1, 19+length(obj.getAlgoName)));
            obj.printOutput(2, '==  Initialize %s  ==', obj.getAlgoName);
            obj.printOutput(2, '%s', repmat('=', 1, 19+length(obj.getAlgoName)));
        end
        
        function obj = printInitClosing(obj)
            obj.printOutput(2, '%s', repmat('=', 1, 19+length(obj.getAlgoName)));
            obj.printOutput(2, '==  Init done  %s  ==', obj.getAlgoName);
            obj.printOutput(2, '%s', repmat('=', 1, 19+length(obj.getAlgoName)));
        end
        
        function obj = printOpening(obj)
            obj.printOutput(2, '');
            obj.printOutput(2, '%s', repmat('=', 1, 19+length(obj.getAlgoName)));
            obj.printOutput(2, '==  Start      %s  ==', obj.getAlgoName);
            obj.printOutput(2, '%s', repmat('=', 1, 19+length(obj.getAlgoName)));
            if obj.s.verbose.mode >= 2
                obj.printParameter();
            end
        end
        
        function obj = printClosing(obj)
            obj.printOutput(2, '%s', repmat('=', 1, 19+length(obj.getAlgoName)));
            obj.printOutput(2, '==  Done       %s  ==', obj.getAlgoName);
            obj.printOutput(2, '%s', repmat('=', 1, 19+length(obj.getAlgoName)));
        end
        
        
        function obj = routineStartup(obj)
        end
        
        function obj = routinePrint(obj)
        end
        
        function obj = routineFinish(obj)
        end
        
        function obj = routineVisualization(obj)
            if obj.s.vis.export.interval > 0
                saveVisualization(obj);
            end
        end
                
        function obj = parseVerbosePattern(obj)
            obj.i.verbose.string = strrep(obj.i.verbose.string, '<default>', obj.i.verbose.defaultPattern);
            
            if isempty(obj.s.eval)
                obj.i.verbose.string = strrep(obj.i.verbose.string, '<eval>', '');
            else
                try
                    obj.i.verbose.string = strrep(obj.i.verbose.string, '<eval>', sprintf('%s', eval(obj.s.eval)));
                catch
                    obj.i.verbose.string = strrep(obj.i.verbose.string, '<eval>', 'eval:SyntaxErr');
                end
            end
            obj.i.verbose.string = strrep(obj.i.verbose.string, '<name>' , sprintf('%s', obj.i.algName));
            obj.i.verbose.string = strrep(obj.i.verbose.string, '<time>' , sprintf('Overall Time: %.5fs', toc(obj.i.overallTime)));
        end
                
        function obj = evaluateCostFunction(obj)            
            obj.i.costOld   = obj.o.currCost;
            obj.o.currCost 	= obj.costFunction(obj.o.result);
            obj.evaluateCustomCostFunction();
        end
        
        function obj = evaluateCustomCostFunction(obj)
        end
        
        
        function obj = printOutput(obj, verbose, s, varargin)
            if obj.s.verbose.mode >= verbose
                if (obj.i.log.fileID > 0)
                    fprintf(obj.i.log.fileID, [obj.s.verbose.prefix, s, '\n'], varargin{:});
                end
                fprintf([obj.s.verbose.prefix, s, '\n'], varargin{:});
            end
        end
        
        function obj = openLogFile(obj)
            %             if obj.s.log
            %                 if isempty(obj.s.log.filePath)
            %                 	obj.s.log.filePath = [obj.i.algName,'.',obj.getParameterString,'.log'];
            %                 end
            %                 if ~any(fopen('all')==obj.i.log.fileID)
            %                     obj.i.log.fileID = fopen(obj.s.log.filePath,'a');
            %                     fprintf(obj.i.log.fileID, obj.listParameter(obj.s.verbose.prefix));
            %                 end
            %             end
        end
        function obj = closeLogFile(obj)
            %             if obj.i.log.fileID > 0
            %                 fclose(obj.i.log.fileID);
            %             end
        end
        
        
        function obj = dispStruct(obj, struct, structName, prefix, seplen)
            if nargin < 4
                prefix = '    ';
            end
            if nargin < 5
                seplen = length(prefix) + length(structName) + oooRoot.dispStructLength(struct) + 1; %max(cellfun(@(x) length(x), allFields));
            end
            allFields = fields(struct);
            
            for j = 1:numel(allFields)
                curField = allFields{j};
                if isempty(struct.(curField))
                    obj.printOutput(2, ['%-',num2str(seplen),'s= [];'], sprintf('%s%s.%s', prefix, structName, curField));
                elseif max(size(struct.(curField)))>1 && ~ischar(struct.(curField))
                    obj.printOutput(2, ['%-',num2str(seplen),'s= array: <%s>;'], sprintf('%s%s%s', prefix, structName, curField), class(struct.(curField)));                    
                else
                    switch class(struct.(curField))
                        case 'struct'
                            obj.printOutput(2, '%-s;', sprintf('%s%s.%s', prefix, structName, curField));
                            obj.dispStruct(struct.(curField), [structName,'.',curField], sprintf('  %s', prefix), seplen);
                        case {'char','string'}
                            obj.printOutput(2, ['%-',num2str(seplen),'s= ''%s'';'], sprintf('%s%s.%s', prefix, structName, curField), struct.(curField));
                        case {'double','single','int8','uint8','int16','uint16','int32','uint32','int64','uint64'}
                            obj.printOutput(2, ['%-',num2str(seplen),'s= %s;'], sprintf('%s%s.%s', prefix, structName, curField), num2str(struct.(curField)));
                        case 'logical'
                            if struct.(curField)
                                obj.printOutput(2, ['%-',num2str(seplen),'s= true;'], sprintf('%s%s.%s', prefix, structName, curField));
                            else
                                obj.printOutput(2, ['%-',num2str(seplen),'s= false;'], sprintf('%s%s.%s', prefix, structName, curField));
                            end
                        case 'function_handle'
                            obj.printOutput(2, ['%-',num2str(seplen),'s= %s;'], sprintf('%s%s.%s', prefix, structName, curField), func2str(struct.(curField)));
                        otherwise
                            obj.printOutput(2, ['%-',num2str(seplen),'s= class: <%s>;'], sprintf('%s%s.%s', prefix, structName, curField), class(struct.(curField)));
                    end
                end
            end
        end
    end
    
    
    %% public methods
    methods (Access = public)
        function result = getResult(obj)
            result = obj.s.resultScaling .* obj.o.result;
        end
        
        
        function name = getUniqueName(obj)
            name = [obj.i.algName,'_',obj.getParameterString(),'_',datestr(obj.i.startTime,'yymmddHHMMss')];
        end
        
        function name = getAlgoName(obj, params)
            if nargin < 2
                params = false;
            end
            if params
                name = [obj.i.algName,'_',obj.getParameterString()];
            else
                name = [obj.i.algName];
            end
        end
        
        function obj = setAlgoName(obj,name)
            obj.i.algName = name;
        end
        
        function obj = setStartValue(obj, x0)
            obj.o.result = x0;
        end
        
        function obj = setReferenceResult(obj, refResult)
            obj.s.refResult = refResult;
        end
        
        function refResult = getReferenceResult(obj)
            refResult = obj.s.refResult;
            if isempty(refResult)
                error('Some internal function require a reference result which is not available: use function setGroundTruth() to set a ground truth');
            end
        end
        function refResult = getReferenceResultVec(obj)
            refResult = obj.s.refResult(:);
            if isempty(refResult)
                error('Some internal function require a reference result which is not available: use function setGroundTruth() to set a ground truth');
            end
        end
        
        
        function obj = setParameterString(obj)
            fnames = fieldnames(obj.p);
            name = cell(1,numel(fnames));
            for k = 1:numel(fnames)
                if k == 1
                    name{k} = [fnames{k}, num2str(obj.p.(fnames{k}))];
                else
                    name{k} = ['_',fnames{k}, num2str(obj.p.(fnames{k}))];
                end
            end
            obj.i.parameterString = cell2mat(name);
        end
        
        function name = getParameterString(obj)
            name = obj.i.parameterString;
        end
        
        function params = getParameter(obj)
            params = obj.p;
        end
        
        function printParameter(obj)
            varName = [inputname(1),'.p'];
            obj.printOutput(2, '==  Current Parameter (obj.p)  ==');
            obj.printOutput(2, '=================================');
            obj.printOutput(2, '');
            obj.dispStruct(obj.p, varName);
            obj.printOutput(2, '');
        end
                
        function setParameter(obj, params)
            obj.parseOptions('p', params);
        end
        
        function settings = getSettings(obj)
            settings = obj.s;
        end
        
        function setSettings(obj, settings)
            obj.parseOptions('s', settings);
        end
        
        function printSettings(obj)
            varName = [inputname(1),'.s'];
            obj.printOutput(2, '==  Current Settings (obj.s)  ==');
            obj.printOutput(2, '================================');
            obj.printOutput(2, '');
            obj.dispStruct(obj.s, varName);
            obj.printOutput(2, '');
        end
                
        function obj = doVisualization(obj)
            
            if isempty(obj.s.vis.cmd)
                obj.printOutput(1, '   !!! WARNING: No visualization defined!');
            else
                if isempty(obj.s.vis.fid) || ~ishandle(obj.s.vis.fid)
                    obj.s.vis.fid = figure('Visible', obj.s.vis.visible);
                end
                set(0, 'CurrentFigure', obj.s.vis.fid);
                obj.s.vis.cmd(obj.getResult());
                if obj.s.vis.showColorbar
                    colorbar;
                end
                drawnow();
            end
        end
        
        
        function obj = saveVisualization(obj, aux)
            
            if isempty(obj.s.vis.cmd)
                obj.printOutput(1, '   !!! WARNING: No visualization defined!');
            else
                if isempty(obj.s.vis.export.name)
                    obj.i.vis.export.name = ['recon_',obj.getUniqueName()];
                else
                    obj.i.vis.export.name = obj.s.vis.export.name;
                end
                if nargin == 2
                    obj.i.vis.export.name = [obj.i.vis.export.name, aux];
                end
                if exist(obj.s.vis.export.path, 'dir')
                    obj.i.vis.export.name = [strip(obj.s.vis.export.path, 'right', '/'), '/', obj.i.vis.export.name];
                end

                if isempty(obj.i.vis.export.fid) || ~ishandle(obj.i.vis.export.fid) 
                    obj.i.vis.export.fid = figure('Visible', false);
                end
                set(0, 'CurrentFigure', obj.i.vis.export.fid);
                set(obj.i.vis.export.fid,'Visible', false);

                if obj.s.useHelperFiles
                    subFigure(111);
                else
                    subplot(111);
                end

                obj.s.vis.customCmd(obj.getResult());
                axis image;
                axis off;
                drawnow();

                if contains(lower(obj.s.vis.export.format), 'png')
                    if obj.s.useHelperFiles
                        resizeFigure(obj.i.vis.export.fid);
                        exportFigure([obj.i.vis.export.name, '.png'], obj.i.vis.export.fid, 'png')
                    else
                        print(obj.i.vis.export.fid,[obj.i.vis.export.name, '.png'],'-dpng','-r0');
                    end
                end
                if contains(lower(obj.s.vis.export.format), 'pdf')
                    print(obj.i.vis.export.fid,[obj.i.vis.export.name, '.pdf'],'-dpdf','-r0');
                end
                if contains(lower(obj.s.vis.export.format), 'fig')
                    save([obj.i.vis.export.name, '.mat'], 'obj.i.vis.export.fid');
                end
            end
        end
    end
    
    
    %% static methods
    methods (Access = protected, Static)       
        function objA = joinStructs(objA, objB)           
            fieldsParams    = fields(objB);           
            for tempI = 1:numel(fieldsParams)
                curField = fieldsParams{tempI};
                if isstruct(objB.(curField)) && isfield(objA, curField)
                    objA.(curField) = oooRoot.joinStructs(objA.(curField), objB.(curField));
                else
                    objA.(curField) = objB.(curField);
                end
            end 
        end 
        
        
        
        function len = dispStructLength(struct)
            allFields   = fields(struct);
            
            potLen      = zeros(1,numel(allFields));
            
            for i = 1:numel(allFields)
                curField = allFields{i};
                if ~isstruct(struct.(curField))
                    potLen(i) = length(curField) + 1;
                else
                    potLen(i) = length(curField) + oooRoot.dispStructLength(struct.(curField)) + 3;
                end
                
            end
            len = max(potLen);
        end
        
    end
    
    
end

if ~exist('inited', 'var')

    addpath('..');
    initOOPAL();
    addpath('~/matlab/helperFiles');
    initHelperFiles();
    addpath('~/matlab/SiMRX');
    initSiMRX();
    addpath('~/matlab/CTTools');
    initCTTools();

    imageSize   = 0.5*[50,50];
    angles      = 3:3:180;
    
    im          = flipXY(createPhantom(imageSize, 'shepplogan3d', 0.1194*imageSize(1).^2));
    
    [A,sinoSize] = genRadon(imageSize(1), angles);
    
    sinogram    = A * im(:);
    sinoNoisy   = sinogram + randn(size(sinogram)) * 0.5;    
    
    
    %%% plot    
    subFigure(245); colormap gray;
    imagesc(im); axis off;
    subFigure(2,4,1,1,2,1); colormap gray;
    imagesc(reshape(sinogram,sinoSize)); axis off;
    subFigure(2,4,3,1,2,1); colormap gray; 
    imagesc(reshape(sinoNoisy,sinoSize)); axis off;
    subFigure(247); colormap gray; axis off;
    subFigure(248); colormap gray; axis off;
    subFigure(246); colormap gray; axis off;
    resizeFigure;
    
    inited = true;
end


%% 
fbp     = iradon(reshape(sinoNoisy, sinoSize),angles);
subFigure(246); colormap gray;
imagesc(fbp,[0,1]); axis off;


%% 
reconBS     = A\sinoNoisy;
subFigure(247); colormap gray;
imagesc(reshape(reconBS, imageSize),[0,1]); axis off;


%%

oTest   = ooL2odataL1grad2D_primalFP(A, sinoNoisy, imageSize);
oTest.p.alpha               = 1e1;
oTest.p.rho                 = 1e1;

oTest.p.tau                 = 1e-4;

oTest.s.vis                 = true;
oTest.s.visFID              = gcf;
oTest.s.visAxes             = subFigure(248);
oTest.s.visIteration        = 0:1:oTest.s.maxIter;
oTest.s.visCustomCmd        = @(x) imagesc(x,[0,1]);

oTest.run();



subFigure(248); colormap gray; axis off;

% figure(4);imagesc(filteredBackprojection);axis image;colormap(gray);title('Iradon Reconstruction');drawnow
% 
% %% Reconstruction and denoising using ROF model
% main = flexBox;
% main.params.tryCPP = 0; %change, if C++ module is compiled
% 
% %add primal var u
% numberU = main.addPrimalVar(size(image));
% 
% %add data-fidelity: 1/2\|Au-f\|_2^2
% main.addTerm(L2dataTermOperator(1,radonMatrix,imageRadonTransformNoise),numberU);
% 
% %add regularizer
% %main.addTerm(L2identity(1,size(image)),numberU); %Tikhonov-regularization
% %main.addTerm(L2gradient(2,size(image)),numberU); %Smoothness
% main.addTerm(L1gradientIso(0.08,size(image)),numberU); %TV-regularization
% 
% 
% % %BEGIN TGV REGULARIZATION
% % numberW1 = main.addPrimalVar(size(image));
% % numberW2 = main.addPrimalVar(size(image));
% % main.addTerm(L1secondOrderGradientIso(0.05,size(image)),[numberU,numberW1,numberW2]);
% % 
% % %create symmetrized gradients
% % gradientBackX = generateBackwardGradND( size(image),[1,1], 1);
% % gradientBackY = generateBackwardGradND( size(image),[1,1], 2);
% % gradientSymmetrized = 0.5 * (gradientBackX + gradientBackY);
% % 
% % main.addTerm(frobeniusOperator(10,2,{gradientBackX,gradientSymmetrized,gradientSymmetrized,gradientBackY}),[numberW1,numberW2]);
% % %END TGV REGULARIZATION
% 
% %box constraint ensures the result to stay in [0,1]
% main.addTerm(boxConstraint(0,1,size(image)),numberU);
% 
% %run minimization algorithm
% tic;main.runAlgorithm;toc;
% 
% %get result
% result = main.getPrimal(numberU);
% 
% %show result
% figure(5);imagesc(result);axis image;colormap(gray);title('Reconstructed Image');

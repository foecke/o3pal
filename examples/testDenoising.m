if ~exist('inited', 'var')

    addpath('..');
    initO3PAL();
    addpath('~/matlab/helperFiles');
    initHelperFiles();

    imageSize   = 50;
    kernelRad   = 1;
    
    cutoutPos   = (ceil(imageSize/4)+1:ceil(imageSize/4)+ceil(imageSize/2))+kernelRad;
    cutout      = repmat(linspace(1,0,ceil(imageSize/2)),[ceil(imageSize/2),1]);
    
    im          = repmat(linspace(-1,1,imageSize+2*kernelRad),[imageSize+2*kernelRad,1]);
    im(cutoutPos,cutoutPos) = cutout;
    imNoisy     = im + randn(size(im)) * 0.05;
    
    blurKernel  = fspecial('gaussian',2*kernelRad+1,2);
    imBlurred   = conv2(imNoisy, blurKernel, 'valid');
    
    im          =      im(kernelRad+1:end-kernelRad,kernelRad+1:end-kernelRad);
    imNoisy     = imNoisy(kernelRad+1:end-kernelRad,kernelRad+1:end-kernelRad);
    
    
    %%% plot   
    inited = true;
end

%%

 
subFigure(241); colormap gray;
imagesc(im); axis off;
subFigure(242); colormap gray;
imagesc(imNoisy); axis off;
subFigure(243); colormap gray;
imagesc(imBlurred); axis off;
subFigure(244); colormap gray; axis off;
subFigure(245); colormap gray; axis off;
resizeFigure;


%%

%im = im2double(rgb2gray(imresize(imread('~/Downloads/IMG_20210317_143735__02.jpg'), [400,400])));

oTest = ooL2dataL1id2D_i_ADMM_pBS(im(:), size(im));
oTest.p.alpha = 1e-1;
oTest.p.nonNeg = false;
oTest.s.tolPrimalDual = 0;
oTest.run();

subFigure(2,4,6); colormap gray;
imagesc(oTest.getResult, [-1,1]); axis off;


% oTest = ooL2dataL1grad2D_i(imBlurred(:), size(im));
% oTest.s.maxIter = 1000;
% oTest.run();
% 
% subFigure(2,4,7); colormap gray;
% imagesc(oTest.getResult); axis off;


%%




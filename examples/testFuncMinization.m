clear;

%% Aim: Minimize f
A = [-1 1; 2 1];
b = [0.5;2];



%% min J

figure(1);
cla;
hold on;

J = @(x) 0.5 .* norm(A*x - b)^2;
gradJ = @(x) A'*A*x - A'*b;


% plot J
N   = 50;
x   = linspace(-5,5,N);

[X,Y] = meshgrid(x,x);
Z = zeros(size(X));
for i=1:N
    for j=1:N
        Z(i,j) = J([X(i,j); Y(i,j)]);
    end
end

surf(X,Y,Z); hold on;


%
hold on;
[xi,res,~] = gradientDescent(J, gradJ, [5;5]);
plot3(xi(1,:), xi(2,:),res+1,'-o','Color','r','MarkerFaceColor','r');

%%

oGD = ooGD(J, gradJ,[2,1]);
oGD.setStartValue([5;5]);
oGD.p.alpha     = 0.17;
oGD.s.record.interval = 1;
oGD.s.record.result = 1;
oGD.run();
resultHistory = [oGD.o.record.result];

plot3(resultHistory(1,:), resultHistory(2,:),[oGD.o.record.cost],'-o','Color','g','MarkerFaceColor','g');



%%


oTSVD = ooL2odataTSVD(A,b,[2,1]);
oTSVD.run();
oTSVD.getResult()

%%

figure(2);
cla;
hold on;


%scaling = normest(A);
% A = A./scaling;
% b = b./scaling;

alpha = 0.0005;
J = @(x) 0.5 .* norm(A*x - b)^2 + alpha .* norm(x)^2;

gradJ = @(x) A'*A*x - A'*b;


% plot J
N   = 101;
x   = linspace(-5,5,N);

[X,Y] = meshgrid(x,x);
Z = zeros(size(X));
for i=1:N
    for j=1:N
        Z(i,j) = J([X(i,j); Y(i,j)]);
    end
end

surf(X,Y,Z); hold on;

%%

oTikhonov = ooL2odataL2idND_BS(A,b,[2,1]);
oTikhonov.run();
oTikhonov.getResult()

%%


%% 

oTikhonovIter = ooL2odataL2idND_i(A,b,[2,1]);
oTikhonovIter.p.alpha = alpha;

oTikhonovIter.p.tau = 0.5;
oTikhonovIter.setStartValue([5;5]);
oTikhonovIter.s.maxIter = 1000;
oTikhonovIter.s.record.interval = 1;
oTikhonovIter.s.record.result = 1;
oTikhonovIter.run();
oTikhonovIter.getResult()

resultHistory = [oTikhonovIter.o.record.result];

plot3(resultHistory(1,:),resultHistory(2,:),[oTikhonovIter.o.record.cost],'-o','Color','g','MarkerFaceColor','g');

%%

oADMMTV2D = ooL2odataL1grad2D_i(A,b,[2,1]);
oADMMTV2D.setStartValue([-5;5]);
oADMMTV2D.p.alpha = alpha;
oADMMTV2D.s.record.interval = 1;
oADMMTV2D.s.record.result = 1;
oADMMTV2D.run();
oADMMTV2D.getResult()

resultHistory = [oADMMTV2D.o.record.result];

plot3(resultHistory(1,:),resultHistory(2,:),[oADMMTV2D.o.record.cost],'-o','Color','g','MarkerFaceColor','g');

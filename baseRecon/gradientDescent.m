function [xi,res,i] = gradientDescent(f, gradF, x0)

    alpha = 0.04343;
    
    
    i       = 1;
    xi(:,i) = x0;
    res(i)  = f(xi(:,i));
    
    while res(end) > 1e-10

        i       = i+1;
        xi(:,i) = xi(:,i-1) - alpha.*gradF(xi(:,i-1));    
        res(i)  = f(xi(:,i));
        
    end
    
end
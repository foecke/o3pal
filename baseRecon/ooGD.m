
%   Author:     Lea Foecke
%   E-Mail:     lea.foecke@uni-muenster.de
%   Institute:  FAU Erlangen-Nuernberg
%   Date:       25-Feb-2020

classdef ooGD < handle & oooIterative
    methods (Access = public)
        function obj = ooGD(J, gradJ, res, varargin)
            
            obj@oooIterative();
            obj.setAlgoName('ooGD');
            obj.i.J     = J;
            obj.i.gradJ = gradJ;
            obj.i.res   = res;
            
            obj.initialize(varargin{:});
        end
        function result = getResult(obj)
            result = reshape(getResult@oooIterative(obj), obj.i.res);
        end
        
        function out = costFunction(obj, x)
            out = obj.i.J(obj.s.resultScaling .* x);
        end
        
    end
    
    
    methods (Access = protected)
        function initializeAlg(obj)
            %obj.p:
            obj.p.alpha     = 0.1;
            %obj.s:            
            %obj.i:
            obj.o.result    = zeros(prod(obj.i.res),1);
        end
        
        function routineUpdateStep(obj)
            obj.o.result  = obj.o.result - obj.p.alpha .* obj.i.gradJ(obj.o.result);
        end
        
        function visualizeResult(obj)
            plot([zeros(size(obj.o.result)),obj.o.result]);
        end
        
    end
    
    
end

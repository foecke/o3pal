
classdef ooL2dataL2grad2D < ooL2dataL2grad2D_BS
        
    methods (Access = public)
        
        
        function obj = ooL2dataL2grad2D(b, res, varargin)
            obj@ooL2dataL2grad2D_BS(b, res, varargin{:});
        end
        
        
    end    
    
end
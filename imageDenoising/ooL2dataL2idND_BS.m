%OOL2DATAL2GRADND
%   min_x -> 1/2 * || x - b ||_2^2 + alpha/2 * || x ||_2^2
%
%   Input:
%       b       2D noisy data
%       res     data resolution (required for gradient)
%
%   Parameter:
%       alpha   regularization parameter
%       tau     step size

%   Author:     Lea Foecke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen
%   Date:       16-Mar-2020

classdef ooL2dataL2idND_BS < oooRoot & oooL2dataL2idND_costFunction
    methods (Access = public)
        function obj = ooL2dataL2idND_BS(b,res, varargin)
            obj@oooRoot();
            obj.setAlgoName('ooL2dataL2idND_BS');
            
            obj.i.b     = b;
            obj.i.res   = res;
            
            obj.initialize(varargin{:});
        end
        
        function result = getResult(obj)
            result = reshape(getResult@oooRoot(obj), obj.i.res);
        end
    end
    
    
    methods (Access = protected)
        
        function initializeAlg(obj)
            obj.p.alpha     = 1;
            obj.p.nonNeg    = false;
            
            obj.i.N     	= prod(obj.i.res);
            obj.i.M       	= numel(obj.i.b);
            obj.o.result    = zeros(obj.i.N,1);
        end
        
        
        function routineUpdateStep(obj)
            obj.setRightHandSide();
            obj.setEffectiveA();
            
            obj.o.result = obj.i.effA \ obj.i.rhs;
            if obj.p.nonNeg
                obj.o.result = max(obj.o.result, 0);
            end
        end
        
        
        function obj = setRightHandSide(obj)
            obj.i.rhs = obj.i.b;
        end
        
        
        function obj = setEffectiveA(obj)
            obj.i.effA = (1 + obj.p.alpha) .* eye(obj.i.N);
        end
    end
end


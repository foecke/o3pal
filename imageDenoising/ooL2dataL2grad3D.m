
classdef ooL2dataL2grad3D < ooL2dataL2grad3D_BS
        
    methods (Access = public)
        
        
        function obj = ooL2dataL2grad3D(b, res, varargin)
            obj@ooL2dataL2grad3D_BS(b, res, varargin{:});
        end
        
        
    end    
    
end
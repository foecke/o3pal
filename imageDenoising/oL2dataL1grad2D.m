function 
alpha       = 1;
rho        	= 1;
tv       	= 'iso';
nonNeg     	= true;


res         = dataOut.res;
N         	= prod(res);
M        	= numel(g);

uAst      	= zeros(N,1);

gradientMats  = genForwardGradient(res, ones(numel(res),1));
gradX         = gradientMats{1};
gradY         = gradientMats{2};

DAD           = gradX'*gradX+gradY'*gradY;  
if nonNeg
    DAD=DAD+speye(N);
end

ATA           = A'*A; 
ATb           = A'*g;

vx            = zeros(N,1);
wx            = zeros(N,1);
vy            = zeros(N,1);
wy            = zeros(N,1);
vp            = zeros(N,1);
wp            = zeros(N,1);

fx            = gradX*uAst;
fy            = gradY*uAst;
fp            = uAst;

vxOld         = vx;
vyOld         = vy;
vpOld         = vp;


effA          = ATA + rho * DAD;  

J             = @(x) 0.5 .* norm(A*x-g).^2 + alpha .* norm(x, 1);

%%


for i = 1:100

    rhs = ATb + rho * (gradX'*(vx(:)-wx(:)) + gradY'*(vy(:)-wy(:)));            
    if nonNeg
        rhs = rhs + rho * (vp(:)-wp(:));
    end

    uAst = effA \ rhs;

    fx = gradX * uAst;
    fy = gradY * uAst;
    fp = uAst;

    vxOld = vx;
    vyOld = vy;
    vpOld = vp;

    if strcmp(tv, 'aniso')
        vx = softThresHold1DVector(fx+wx,alpha/rho);
        vy = softThresHold1DVector(fy+wy,alpha/rho);
    else
        [vx,vy] = softThresHold2DVector(fx+wx,fy+wy,alpha/rho);
    end
    vp = max(wp+fp, 0);

    wx = wx + fx - vx;
    wy = wy + fy - vy;
    wp = wp + fp - vp;

    if mod(i, 1) == 0 || curI == kb
        useFigure(fid5);
        imagesc(reshape(uAst, dataOut.res), dataOut.u.cLim);
        drawnow;
        fprintf('#');
    end
end
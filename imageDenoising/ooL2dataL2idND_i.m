%OOL2DATAL2GRADND
%   min_x -> 1/2 * || x - b ||_2^2 + alpha/2 * || x ||_2^2
%
%   Input:
%       b       2D noisy data
%       res     data resolution (required for gradient)
%
%   Parameter:
%       alpha   regularization parameter
%       tau     step size

%   Author:     Lea Foecke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen
%   Date:       16-Mar-2020

classdef ooL2dataL2idND_i < oooIterative
    methods (Access = public)
        function obj = ooL2dataL2idND_i(b,res, varargin)
            obj@oooIterative();
            obj.setAlgoName('ooL2dataL2idND_i');
            
            obj.i.b     = b;
            obj.i.res   = res;
            
            obj.initialize(varargin{:});
        end
        
        function result = getResult(obj)
            result = reshape(getResult@oooRoot(obj), obj.i.res);
        end
        
        function costFunction(obj)
            obj.o.currCost = (0.5 .* norm(obj.o.result-obj.i.b).^2 + 0.5 .* obj.p.alpha .* norm(obj.o.result).^2) ./ numel(obj.o.result);
        end
    end
    
    
    methods (Access = protected)
        
        function initializeAlg(obj)
            obj.p.alpha     = 1;
            obj.p.gamma     = 0.1;
            obj.p.nonNeg    = false;
            
            obj.i.N     	= prod(obj.i.res);
            obj.i.M       	= numel(obj.i.b);
            obj.o.result    = zeros(obj.i.N,1);
        end
        
        function routineUpdateStep(obj)
            obj.o.result = obj.o.result - obj.p.gamma .* ((1+obj.p.alpha) .* obj.o.result - obj.i.b);
        end
    end
end


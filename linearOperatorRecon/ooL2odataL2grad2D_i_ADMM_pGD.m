
classdef ooL2odataL2grad2D_i_ADMM_pGD < ooL2odataL2grad2D_i_ADMM_pBS
    methods (Access = public)
        function obj = ooL2odataL2grad2D_i_ADMM_pGD(A, b ,res, varargin)
            obj@ooL2odataL2grad2D_i_ADMM_pBS(A, b, res, varargin{:});
            obj.setAlgoName('ooL2odataL2grad2D_i_ADMM_pGD');
        end
        
        
    end
    
    methods (Access = protected)
        
        
        function initializeAlg(obj)
            
            initializeAlg@oooADMM(obj);
            
            obj.p.alpha        	= 1e-1;
            obj.p.rho        	= 1e-2;
            obj.p.nonNeg        = true;
            
            obj.i.N             = prod(obj.i.res);
            obj.i.M             = numel(obj.i.b);
            obj.o.result        = zeros(obj.i.N,1);
            
            obj.i.ATA           = obj.i.A'*obj.i.A;
            obj.i.ATb           = obj.i.A'*obj.i.b;
            
            gradientMats        = genForwardGradient(obj.i.res, ones(numel(obj.i.res),1));
            obj.i.gradX         = gradientMats{1};
            obj.i.gradY         = gradientMats{2};
            obj.i.DAD           = obj.i.gradX'*obj.i.gradX+obj.i.gradY'*obj.i.gradY;
            
            obj.i.fp            = obj.o.result;
            
            obj.i.vp            = zeros(obj.i.N,1);
            obj.i.vpOld         = obj.i.vp;
            
            obj.i.wp            = zeros(obj.i.N,1);
            
        end
        
        
        function routineUpdateStep(obj)
            obj.setEffectiveA();
            obj.setRightHandSide();
            
            obj.o.result    = obj.o.result - obj.p.gamma * (obj.i.effA - obj.i.rhs);
            
            obj.i.fp        = obj.o.result;            
            obj.i.vpOld = obj.i.vp;
            obj.i.vp    = max(obj.i.wp+obj.i.fp, 0);            
            obj.i.wp = obj.i.wp + obj.i.fp - obj.i.vp;
            
        end
        
        
        function obj = adaptiveStep(obj)
            obj = adaptiveStep@oooADMM(obj);
            obj.i.wp = obj.i.wp*(obj.i.rhoOld/obj.p.rho);
        end
        
        
        function obj = setRightHandSide(obj) 
            if obj.p.nonNeg
                obj.i.rhs   = obj.i.ATb + obj.p.rho .* (obj.i.vp(:)-obj.i.wp(:));
            else
                obj.i.rhs   = obj.i.ATb;
            end
        end
        
        
        function obj = setEffectiveA(obj) 
            if obj.p.nonNeg
                obj.i.effA  = obj.i.A'*(obj.i.A * obj.o.result) + obj.p.alpha .* ( obj.i.gradX' * (obj.i.gradX * obj.o.result) + obj.i.gradY' * (obj.i.gradY * obj.o.result) ) + obj.p.rho .* obj.o.result;
            else
                obj.i.effA  = obj.i.A'*(obj.i.A * obj.o.result) + obj.p.alpha .* ( obj.i.gradX' * (obj.i.gradX * obj.o.result) + obj.i.gradY' * (obj.i.gradY * obj.o.result) );
            end
        end
        
        
    end
    
end


classdef ooL2odataL2idND_i_GD_AAT < ooL2odataL2idND_i_GD
    
    methods (Access = public)
        
        
        function obj = ooL2odataL2idND_i_GD_AAT(A,b,res, varargin)
            obj@ooL2odataL2idND_i_GD(A,b,res, varargin{:});
            obj.setAlgoName('ooL2odataL2idND_i_GD_AAT');
        end  
        
        
    end
    
    methods (Access = protected)   
        
        
        function obj = initializeAlg(obj)
            obj.p.alpha     = 1;
            obj.p.gamma     = 1;
            obj.p.nonNeg    = false;
            
            obj.i.N     	= prod(obj.i.res);
            obj.i.M       	= numel(obj.i.b);
            obj.o.result    = zeros(obj.i.N,1);
            
            obj.i.ATAATIB   = obj.i.A' * ( (obj.i.A*obj.i.A') \ obj.i.b);
        end
        
        
        function routineUpdateStep(obj) 
%             obj.setRightHandSide();
%             % This formula has to be validated!
%             obj.o.result = obj.i.A' * (obj.i.effA \ obj.i.rhs);  
            obj.o.result = (1 + obj.p.gamma) .* obj.o.result - obj.p.alpha .* obj.o.result - obj.i.ATAATIB;
            if obj.p.nonNeg
                obj.o.result = max(obj.o.result, 0); 
            end  
        end
        
        
        function obj = setEffectiveA(obj)
%             obj.i.effA = obj.i.A * obj.i.A' + obj.p.alpha.*speye(obj.i.M) + obj.p.alpha.*speye(obj.i.M);
        end
        
        
        function obj = setRightHandSide(obj)
%             obj.i.rhs = obj.i.b + obj.p.alpha.* (obj.i.A * obj.o.result);
        end
        
        
    end   
    
end

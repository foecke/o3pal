%OOL2DATAL2GRAD3D
%   min_x -> 1/2 * || Ax - b ||_2^2 + alpha/2 * || \nabla x ||_2^2
%
%   Input:
%       A       linear operator (matrix)
%       b       3D noisy data
%       res     data resolution (required for gradient)
%
%   Parameter:
%       alpha   regularization parameter
%       tau     step size

%   Author:     Lea Foecke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen
%   Date:       16-Mar-2020

classdef ooL2odataL2grad3D_i_GD < oooIterative
    methods (Access = public)
        function obj = ooL2odataL2grad3D_i_GD(A, b, res, varargin)
            obj@oooIterative();
            obj.setAlgoName('ooL2odataL2grad3D_i_GD');
            
            obj.i.A     = A;
            obj.i.b     = b;
            obj.i.res   = res;
            
            obj.initialize(varargin{:});
        end
        
        function result = getResult(obj)
            result = reshape(getResult@oooRoot(obj), obj.i.res);
        end
        
        function out = costFunction(obj, x)
            out = (0.5 .* norm(obj.i.A*x(:)-obj.i.b).^2 + 0.5 .* obj.p.alpha .* norm(obj.i.gradX*x(:) + obj.i.gradY*x(:) + obj.i.gradZ*x(:)).^2) ./ numel(x(:));
        end
    end
    
    
    methods (Access = protected)
        
        function initializeAlg(obj)
            obj.p.alpha     = 1;
            obj.p.gamma     = 0.1;
            obj.p.nonNeg    = false;
            
            obj.i.N     	= prod(obj.i.res);
            obj.i.M       	= numel(obj.i.b);
            obj.o.result    = zeros(obj.i.N,1);
            
            gradientMats    = genForwardGradient(obj.i.res, ones(numel(obj.i.res), 1));
            obj.i.gradX     = gradientMats{1};
            obj.i.gradY     = gradientMats{2};
            obj.i.gradZ     = gradientMats{3};
            
            obj.i.Atb       = obj.i.A' * obj.i.b;
        end
        
        
        function routineUpdateStep(obj)
            obj.o.result = obj.o.result - obj.p.gamma .* (obj.i.A'*(obj.i.A*obj.o.result) - obj.i.Atb + obj.p.alpha .* (obj.i.gradX'*(obj.i.gradX*obj.o.result) + obj.i.gradY'*(obj.i.gradY*obj.o.result) + obj.i.gradZ'*(obj.i.gradZ*obj.o.result)));
        end
    end
end


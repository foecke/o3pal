
classdef ooL2odataL2grad2D_BS < oooRoot & oooL2odataL2grad2D_costFunction
        
    methods (Access = public)
        
        
        function obj = ooL2odataL2grad2D_BS(A, b, res, varargin)
            obj@oooRoot();
            obj.setAlgoName('ooL2odataL2grad2D_BS');
            
            obj.i.A     = A;
            obj.i.b     = b;
            obj.i.res   = res;
            
            obj.initialize(varargin{:});
        end
        
        
        function result = getResult(obj)
            result = reshape(getResult@oooRoot(obj), obj.i.res);
        end
        
        
    end    
    
    methods (Access = protected)
        
        
        function initializeAlg(obj)
            obj.p.alpha     = 1;
            obj.p.nonNeg    = false;
            
            obj.i.N     	= prod(obj.i.res);
            obj.i.M       	= numel(obj.i.b);
            obj.o.result    = zeros(obj.i.N,1);
            
            gradientMats    = genForwardGradient(obj.i.res, ones(2,1));
            obj.i.gradX     = gradientMats{1};
            obj.i.gradY     = gradientMats{2};
        end
        
        
        function routineUpdateStep(obj)
            obj.setRightHandSide();
            obj.setEffectiveA();
            
            obj.o.result = obj.i.effA \ obj.i.rhs;
            if obj.p.nonNeg
                obj.o.result = max(obj.o.result, 0);
            end
        end
        
        
        function obj = setRightHandSide(obj)
            obj.i.rhs = obj.i.A' * obj.i.b;
        end
        
        
        function obj = setEffectiveA(obj)
            obj.i.effA = obj.i.A'*obj.i.A + obj.p.alpha .* ( obj.i.gradX'*obj.i.gradX + obj.i.gradY'*obj.i.gradY );
        end
        
        
    end    
    
end

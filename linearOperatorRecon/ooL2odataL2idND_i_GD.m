
classdef ooL2odataL2idND_i_GD < oooIterative & oooL2odataL2idND_costFunction
    
    methods (Access = public)
        
        
        function obj = ooL2odataL2idND_i_GD(A, b, res, varargin)
            obj@oooIterative();
            obj.setAlgoName('ooL2odataL2idND_i_GD');
            
            obj.i.A     = A;
            obj.i.b     = b;
            obj.i.res   = res;
            
            obj.initialize(varargin{:});
        end
        
        
        function result = getResult(obj)
            result = reshape(getResult@oooRoot(obj), obj.i.res);
        end
        
        
    end
    
    methods (Access = protected)
         
        
        function obj = initializeAlg(obj)
            obj.p.alpha     = 1;            
            obj.p.gamma     = 1;
            obj.p.nonNeg    = false;
            
            obj.s.adaptive.interval = 0;
            
            obj.i.N     	= prod(obj.i.res);
            obj.i.M       	= numel(obj.i.b);
            obj.o.result    = zeros(obj.i.N,1);
            
            obj.i.ATb       = obj.i.A' * obj.i.b;
        end
        
        
        function obj = routineUpdateStep(obj)
            obj.o.result = obj.o.result - obj.p.gamma .* ( obj.i.A'*(obj.i.A*obj.o.result) - obj.i.ATb + obj.p.alpha .* obj.o.result );
            if obj.p.nonNeg
                obj.o.result = max(obj.o.result, 0); 
            end
        end
        
        
    end
    
end

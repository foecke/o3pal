
classdef ooL2odataL2idND_i_ADMM_pBS < oooADMM & oooL2odataL2idND_costFunction
    methods (Access = public)
        function obj = ooL2odataL2idND_i_ADMM_pBS(A, b ,res, varargin)
            obj@oooADMM();
            obj.setAlgoName('ooL2odataL2idND_i_ADMM_pBS');
            
            obj.i.A     = A;
            obj.i.b     = b;
            obj.i.res   = res;
            
            obj.initialize(varargin{:});
        end
        
        
        function result = getResult(obj)
            result = reshape(getResult@oooRoot(obj), obj.i.res);
        end
        
        
    end
    
    methods (Access = protected)
        
        
        function obj = initializeAlg(obj)  
            initializeAlg@oooADMM(obj); 
            
            obj.p.alpha     = 1e-1;
            obj.p.rho     	= 1e-2;
            obj.p.nonNeg   	= true;
            
            obj.i.N       	= prod(obj.i.res);
            obj.i.M       	= numel(obj.i.b);
            obj.o.result   	= zeros(obj.i.N,1);
            
            obj.i.ATA      	= obj.i.A'*obj.i.A; 
            obj.i.ATb    	= obj.i.A'*obj.i.b;
            
            obj.i.fp     	= zeros(obj.i.N,1);
            obj.i.vp     	= zeros(obj.i.N,1);
            obj.i.vpOld    	= zeros(obj.i.N,1);
            obj.i.wp      	= zeros(obj.i.N,1);
        end
        
        
        function obj = routineStartup(obj) 
            obj.setEffectiveA();
        end
        
        
        function obj = routineUpdateStep(obj)
            obj.setRightHandSide();
            obj.o.result 	= obj.i.effA \ obj.i.rhs;
            
            obj.i.fp        = obj.o.result;
            obj.i.vpOld     = obj.i.vp;
            obj.i.vp        = max(obj.i.wp+obj.i.fp, 0);
            obj.i.wp        = obj.i.wp + obj.i.fp - obj.i.vp;
        end
        
        
        function obj = evaluateCustomCostFunction(obj)
            if obj.p.nonNeg
                obj.o.primalErr = norm(obj.i.fp-obj.i.vp) ./ obj.i.N;
                obj.o.dualErr   = norm(-obj.p.rho * (obj.i.vp-obj.i.vpOld)) ./ obj.i.N;                
            else
                obj.o.primalErr = 0;
                obj.o.dualErr   = 0;
            end
            obj.o.nnegErr   = norm(obj.o.result(obj.o.result<0), inf)/norm(obj.o.result(obj.o.result>0));
        end
        
        
        function obj = adaptiveStep(obj)            
            obj = adaptiveStep@oooADMM(obj);
            obj.i.wp = obj.i.wp*(obj.i.rhoOld/obj.p.rho);
            obj.setEffectiveA();
        end
        
        
        function obj = setRightHandSide(obj) 
            if obj.p.nonNeg
                obj.i.rhs = obj.i.ATb + obj.p.rho .* (obj.i.vp(:)-obj.i.wp(:));
            else
                obj.i.rhs = obj.i.ATb;                
            end
        end
        
        
        function obj = setEffectiveA(obj) 
            if obj.p.nonNeg      
                obj.i.effA = obj.i.ATA + (obj.p.alpha + obj.p.rho) .* speye(obj.i.N); 
            else         
                obj.i.effA = obj.i.ATA + obj.p.alpha * speye(obj.i.N); 
            end   
        end
        
        
    end
    
end


classdef ooL2odataTSVD < oooRoot & oooL2odata_costFunction
    
    methods (Access = public)
        
        
        function obj = ooL2odataTSVD(A, b, res, varargin)
            obj@oooRoot();
            obj.setAlgoName('ooL2odataTSVD');
            
            obj.i.A     = A;
            obj.i.b     = b;
            obj.i.res   = res;
            
            obj.initialize(varargin{:});
        end
        
        
        function result = getResult(obj)
            result = reshape(getResult@oooRoot(obj), obj.i.res);
        end
                
    end
    
    methods (Access = protected)
        
        
        function initializeAlg(obj)
            obj.p.K            	= 20;
            obj.i.Keff         	= obj.p.K;
            
            if issparse(obj.i.A)
                [obj.i.U,obj.i.S,obj.i.V]   = svds(obj.i.A);
            else
                [obj.i.U,obj.i.S,obj.i.V]   = svd(obj.i.A);
            end
            %obj.i.pseudoS               = diag(1./diag(obj.i.S));
            
            eigVRange           = find(abs(diag(obj.i.S)) > 1e-16);
            
            obj.i.pseudoS       = zeros(size(obj.i.S));
            pSDiag              = diag(obj.i.pseudoS);
            pSDiag(eigVRange)   = 1./diag(obj.i.S(eigVRange,eigVRange));
            obj.i.pseudoS       = diag(pSDiag);
        end
        
        
        function routineUpdateStep(obj)
            obj.i.KMaxSize = min(size(obj.i.S));
            if obj.p.K <= 0
                obj.i.Keff = obj.i.KMaxSize;
            elseif obj.p.K < 1
                obj.i.Keff = ceil(obj.i.KMaxSize * obj.p.K);
            else
                obj.i.Keff = min(obj.p.K,obj.i.KMaxSize);
            end
            obj.o.result = obj.i.V(:,1:obj.i.Keff)*obj.i.pseudoS(1:obj.i.Keff,1:obj.i.Keff)*obj.i.U(:,1:obj.i.Keff)'*obj.i.b;
        end
        
        
        function routinePrint(obj)
            if obj.p.K <= 0
                printOutput(obj, 2, 'use all (%i)\n', obj.i.KMaxSize);
            elseif obj.p.K < 1
                printOutput(obj, 2, 'use %i/%i\n',obj.i.Keff,obj.i.KMaxSize);
            else
                printOutput(obj, 2, 'use the first %i\n',obj.i.Keff);
            end
        end
        
        
    end
    
end


classdef ooL2odataL1grad2D_i < ooL2odataL1grad2D_i_ADMM_pBS
        
    methods (Access = public)
        
        
        function obj = ooL2odataL1grad2D_i(A, b, res, varargin)
            obj@ooL2odataL1grad2D_i_ADMM_pBS(A, b, res, varargin{:});
        end
        
        
    end    
    
end


classdef ooL2odataL2idND_BS_AAT < ooL2odataL2idND_BS
    
    methods (Access = public)
        
        
        function obj = ooL2odataL2idND_BS_AAT(A, b, res, varargin)
            obj@ooL2odataL2idND_BS(A, b, res, varargin{:});
            obj.setAlgoName('ooL2odataL2idND_BS_AAT');
        end
        
        
    end
    
    methods (Access = protected)
        
        
        function obj = initializeAlg(obj)
            obj.p.alpha     = 1;
            
            obj.i.N     	= prod(obj.i.res);
            obj.i.M       	= numel(obj.i.b);
            obj.o.result    = zeros(obj.i.N,1);
            
            obj.i.AAT       = obj.i.A*obj.i.A';
        end
        
        
        function obj = routineUpdateStep(obj)
            obj.o.result = obj.i.A' * ( ( obj.i.AAT + obj.p.alpha .* speye(obj.i.M) ) \ obj.i.b );
        end
        
        
    end
    
end

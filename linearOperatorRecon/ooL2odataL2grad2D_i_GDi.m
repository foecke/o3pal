
classdef ooL2odataL2grad2D_i_GDi < ooL2odataL2grad2D_i_GD
    
    methods (Access = public)
        
        
        function obj = ooL2odataL2grad2D_i_GDi(A, b, res, varargin)
            obj@ooL2odataL2grad2D_i_GD(A, b, res, varargin{:});
            obj.setAlgoName('ooL2odataL2grad2D_i_GDi');
        end
        
        
    end
    
    methods (Access = protected)
        
        
        function initializeAlg(obj)
            obj.p.alpha     = 1;            
            obj.p.gamma     = 1;
            obj.p.nonNeg    = false;
            
            obj.i.N     	= prod(obj.i.res);
            obj.i.M       	= numel(obj.i.b);
            obj.o.result    = zeros(obj.i.N,1);
            
            gradientMats    = genForwardGradient(obj.i.res, ones(numel(obj.i.res),1));
            obj.i.gradX     = gradientMats{1};
            obj.i.gradY     = gradientMats{2};
            
            obj.i.ATb       = obj.i.A' * obj.i.b;
        end
        
        
        function obj = routineStartup(obj)  
            obj.setEffectiveA();
        end
        
        
        function obj = routineUpdateStep(obj)
            obj.setRightHandSide();
            
            obj.o.result = obj.i.effA \ obj.i.rhs;
            if obj.p.nonNeg
                obj.o.result = max(obj.o.result, 0); 
            end
        end
        
        
        function obj = setEffectiveA(obj)
            obj.i.effA = obj.p.gamma .* ( obj.i.A' * obj.i.A + obj.p.alpha .* (obj.i.gradX'*obj.i.gradX + obj.i.gradY'*obj.i.gradY)) + speye(obj.i.N);
        end
        
        
        function obj = setRightHandSide(obj)
            obj.i.rhs = obj.p.gamma .* obj.i.ATb + obj.o.result;
        end
        
        
    end
    
end

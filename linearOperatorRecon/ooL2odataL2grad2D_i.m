
classdef ooL2odataL2grad2D_i < ooL2odataL2grad2D_i_GD    
    
    methods (Access = public)
        
        
        function obj = ooL2odataL2grad2D_i(A, b, res, varargin)
            obj@ooL2odataL2grad2D_i_GD(A, b, res, varargin{:});
        end
        
        
    end    
    
end

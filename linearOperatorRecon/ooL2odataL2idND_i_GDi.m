
% implicit scheme: 
%                                                    u^{k+1} = u^k - \nabla F(u^{k+1})
% \leftrightarrow                                    u^{k+1} = u^k - A^\ast Au^{k+1} + \alpha u^{k+1} + A^\ast b
% \leftrightarrow u^{k+1} + \alpha u^{k+1} + A^\ast Au^{k+1} = u^k + A^\ast b
% \leftrightarrow                                    u^{k+1} = (1_n + \alpha\cdot 1_n + A^\ast A)^{-1} (u^k + A^\ast b)
            
%   Author:     Lea Foecke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen
%   Date:       13-Oct-2017

classdef ooL2odataL2idND_i_GDi < ooL2odataL2idND_i_GD
    
    methods (Access = public)
        
        
        function obj = ooL2odataL2idND_i_GDi(A, b, res, varargin)
            obj@ooL2odataL2idND_i_GD(A, b, res, varargin{:});
            obj.setAlgoName('ooL2odataL2idND_i_GDi');
        end
        
        
    end
    
    methods (Access = protected)
        
        
        function obj = routineStartup(obj)  
            obj.setEffectiveA();
        end
        
        
        function obj = initializeAlg(obj)
            obj.p.alpha     = 1;       
            obj.p.nonNeg    = false;
            
            obj.i.N     	= prod(obj.i.res);
            obj.i.M       	= numel(obj.i.b);
            obj.o.result    = zeros(obj.i.N,1);
            
            obj.i.ATb       = obj.i.A' * obj.i.b;
        end
        
        
        function obj = routineUpdateStep(obj)
            obj.setRightHandSide();
            
            obj.o.result = obj.i.effA \ obj.i.rhs;
            if obj.p.nonNeg
                obj.o.result = max(obj.o.result, 0); 
            end
        end
        
        
        function obj = setEffectiveA(obj)
            obj.i.effA = obj.p.gamma .* ( obj.i.A' * obj.i.A + obj.p.alpha .* speye(obj.i.N) ) + speye(obj.i.N);
        end
        
        
        function obj = setRightHandSide(obj)
            obj.i.rhs = obj.p.gamma .* obj.i.ATb + obj.o.result;
        end
        
        
    end
    
end

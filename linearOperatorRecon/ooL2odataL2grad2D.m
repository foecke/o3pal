
classdef ooL2odataL2grad2D < ooL2odataL2grad2D_BS
        
    methods (Access = public)
        
        
        function obj = ooL2odataL2grad2D(A, b, res, varargin)
            obj@ooL2odataL2grad2D_BS(A, b, res, varargin{:});
        end
        
        
    end    
    
end

%OOL2DATAL2GRAD2D
%   min_x -> 2*x
%
%   Input:
%
%   Parameter:

%   Author:     Lea Foecke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen
%   Date:       17-Mar-2020

classdef ooExample < handle & ooIterative
    methods (Access = public)
        function obj = ooExample(varargin)
            obj@ooIterative();
            obj.setAlgoName('ooExample');
            
            obj.initialize(varargin{:});
        end
        
        function result = getResult(obj)
            result          = obj.o.result;
        end
    end
    
    methods (Access = protected)
        
        function initializeAlg(obj)
            obj.o.result    = 1;
        end
        
        function costFunction(obj)
            obj.o.currCost  = norm(obj.o.result) ./ numel(obj.o.result);
        end
        
        function routineUpdateStep(obj)
            obj.o.result    = 0.95.*obj.o.result;
        end
    end    
end


